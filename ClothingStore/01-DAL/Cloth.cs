﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Seldat {
    public class Cloth {
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity), Key()]
        public int ClothId { get; set; }
        [Required]
        [ForeignKey("Category")]
        public int CategoryId { get; set; }
        [Required]
        [ForeignKey("Company")]
        public int CompanyId { get; set; }
        [Required]
        [ForeignKey("Item")]
        public int ItemId { get; set; }
        [Required]
        public decimal Price { get; set; }
        public decimal?Discount { get; set; }
        [Required]
        public string Image { get; set; }

        public virtual Category Category { get; set; }
        public virtual Company Company { get; set; }
        public virtual Item Item { get; set; }



    }
}
