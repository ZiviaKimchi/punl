﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Cors;
namespace Seldat {
    [EnableCors("*", "*", "*")]
    public class ClothApiController : ApiController {

        ClothLogic logic = new ClothLogic();

        [HttpGet]
        [Route("api/CategoryClothes/{id}")]
        public HttpResponseMessage GetCategoryClothes(int id) { 
            try {

                List<ClothModel> clothes= logic.GetCategoryClothes(id);
                
                return Request.CreateResponse(HttpStatusCode.OK, clothes);
            }
            catch (Exception ex) {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.GetUserFreindlyMessage());
            }
        }

        [HttpGet]
        [Route("api/CategoryClothesSale/{id}")]
        public HttpResponseMessage GetCategoryClothesSale(int id) {
            try {

                List<ClothModel> clothes = logic.GetCategoryClothesSale(id);

                return Request.CreateResponse(HttpStatusCode.OK, clothes);
            }
            catch (Exception ex) {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.GetUserFreindlyMessage());
            }
        }

        [HttpGet]
        [Route("api/clothes/{clothId}")]
        public HttpResponseMessage GetOneCloth(int clothId) {
            try {
                ClothModel cloth = logic.GetOneCloth(clothId);
                return Request.CreateResponse(HttpStatusCode.OK, cloth);
            }
            catch (Exception ex) {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.GetUserFreindlyMessage());
            }
        }

        [HttpPost]
        [Route("api/clothes")]
        public HttpResponseMessage AddCloth(ClothModel model) {
            try {
                if (!ModelState.IsValid)
                    return Request.CreateResponse(HttpStatusCode.BadRequest, ModelState.GetAllError());
                return Request.CreateResponse(HttpStatusCode.OK, logic.AddCloth(model));
            }
            catch (Exception ex) {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.GetUserFreindlyMessage());
            }
        }

        [HttpPut]
        [Route("api/clothes/{id}")]
        public HttpResponseMessage UpdateCloth([FromUri]int id, [FromBody]ClothModel model) {
            try {
                if (!ModelState.IsValid) {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, ModelState.GetAllError());
                }
                model.id = id;
                return Request.CreateResponse(HttpStatusCode.OK, logic.UpdateFullCloth(model));
            }
            catch (Exception ex) {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.GetUserFreindlyMessage());
            }
        }

        [HttpDelete]
        [Route("api/clothes/{id}")]
        public HttpResponseMessage DeleteCloth(int id) {
            try {
                logic.RemoveCloth(id);
                return Request.CreateResponse(HttpStatusCode.OK);
            }
            catch (Exception ex) {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.GetUserFreindlyMessage());
            }
        }

        [HttpPost]
        [Route("api/upload")]
        public HttpResponseMessage uploadImage() {
            string newFileName = "";
            try {
                int id = int.Parse(HttpContext.Current.Request.Form["clothingID"]);
                if (HttpContext.Current.Request.Files.Count > 0) {
                    string originalName = HttpContext.Current.Request.Files[0].FileName;
                    newFileName = Guid.NewGuid().ToString() + Path.GetExtension(originalName);
                    string fullPathAndFileName = HttpContext.Current.Server.MapPath("~/Images/" + newFileName);
                    HttpContext.Current.Request.Files[0].SaveAs(fullPathAndFileName);
                }
                logic.updateImage(id, newFileName);
                return Request.CreateResponse(HttpStatusCode.Created, id);
            }
            catch (Exception ex) {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.Message);
            }
        }

        protected override void Dispose(bool disposing) {
            if (disposing)
                logic.Dispose();
            base.Dispose(disposing);
        }
    }
}
