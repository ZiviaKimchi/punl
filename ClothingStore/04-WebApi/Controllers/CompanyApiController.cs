﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace Seldat {
    [EnableCors("*", "*", "*")]
    public class CompanyApiController : ApiController {

        CompanyLogic logic = new CompanyLogic();

        [HttpGet]
        [Route("api/company")]
        public HttpResponseMessage GetAllCompanies() {
            try {
                List<CompanyModel> companies = logic.GetAllCompanies();
                return Request.CreateResponse(HttpStatusCode.OK, companies);
            }
            catch (Exception ex) {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.GetUserFreindlyMessage());
            }
        }

        protected override void Dispose(bool disposing) {
            if (disposing)
                logic.Dispose();
            base.Dispose(disposing);
        }
    }
}
