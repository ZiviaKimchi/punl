﻿using System.Collections.Generic;
using System.Linq;

namespace Seldat {
    public class CompanyLogic : BaseLogic {

        public List<CompanyModel> GetAllCompanies() {
            return db.Companies.Select(c => new CompanyModel { id = c.CompanyId, name = c.CompanyName }).ToList();
        }
    }
}
