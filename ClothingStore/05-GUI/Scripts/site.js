﻿function showCategoryClothes(categoryId) {
    $.ajax({
        method: "GET",
        url: "http://localhost:58792/api/CategoryClothes/" + categoryId,
        cache: false,
        error: function (err) {
            var errors = JSON.parse(err.responseText);
            var error = "";
            for (var i in errors)
                error += errors[i] + " ";
            alert(err.status + ", " + err.statusText + "----" + error);
        },
        success: function (response) {
            $("#containbody").empty();
            for (var i = 0; i < response.length; i++) {
                $("#containbody").append("<a href='#myPopup' data-rel='popup' data-transition='slide' class='ui-btn ui-btn-inline ui-corner-all' onclick='showDetails(" + response[i].id + ")'><img src='http://localhost:58792/Images/" + response[i].image + "' /></a><div>");
            }
        }
    });
}

function showDetails(clothId) {
    $.ajax({
        method: "GET",
        url: "http://localhost:58792/api/clothes/" + clothId,
        cache: false,
        error: function (err) {
            var errors = JSON.parse(err.responseText);
            var error = "";
            for (var i in errors)
                error += errors[i] + " ";
            alert(err.status + ", " + err.statusText + "----" + error);
        },
        success: function (response) {
            var discount = response.discount;
            if (discount == null)
                discount = 0;
            $("#myPopup").empty();
            $("#myPopup").append("<p>" + response.item.name + " " + response.category.name + " " + response.company.name + " </br>מחיר:" + response.price + " </br>אחוזי הנחה:" + discount + "</p>");
        }
    });
}

function showAllCategories() {
    $.ajax({
        method: "GET",
        url: "http://localhost:58792/api/category",
        cache: false,
        error: function (err) {
            var errors = JSON.parse(err.responseText);
            var error = "";
            for (var i in errors)
                error += errors[i] + " ";
            alert(err.status + ", " + err.statusText + "----" + error);
        },
        success: function (response) {
            for (i = 0; i < response.length; i++) {
                $("#containbuttons").append("<input type='button' class='allOption' value='" + response[i].name + "' onclick='showCategoryClothes(" + response[i].id + ")'></button>");
            }
        }
    });
}

function login() {
    if (sessionStorage.getItem("user")) {
        window.location.href = "admin.html";
        return;
    }
    window.location.href = "login.html";
}
