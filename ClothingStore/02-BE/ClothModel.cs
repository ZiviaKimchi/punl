﻿using System.ComponentModel.DataAnnotations;

namespace Seldat {
    public class ClothModel {

        public int id { get; set; }

        [Required(ErrorMessage = "Category is missing")]
        public CategoryModel category { get; set; }

        [Required(ErrorMessage = "Company is missing")]
        public CompanyModel company { get; set; }

        [Required(ErrorMessage = "Item is missing")]
        public ItemModel item { get; set; }

        [Required(ErrorMessage = "Price is missing")]
        public decimal price { get; set; }

        public decimal? discount { get; set; }

        [Required(ErrorMessage = "Image is missing")]
        public string image { get; set; }
    }
}
